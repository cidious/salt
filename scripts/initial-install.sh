#!/bin/bash

# расшифровываем персональный ключ для gitlab
if [ ! -f ssh/id_rsa ] ; then
  gpg ssh/id_rsa.gpg
fi
mv ssh/id_rsa ~/.ssh/
chmod 0600 ~/.ssh/id_rsa
# копируем ключ в salt чтобы на миньонах подключаться к gitlab
cp -a ~/.ssh/id_rsa /srv/salt/id_rsa
chmod 0444 /srv/salt/id_rsa

# расшифровываем коннект к Wireguard VPN
gpg wireguard/uahosting.conf.gpg
mv wireguard/uahosting.conf /etc/wireguard/

# прописываем настроенный конфиг для солта
cp -f salt/master /etc/salt/
systemctl restart salt-master

# ставим salt-api чтобы заработала админка SaltGUI
pip install contextvars
systemctl restart salt-api
selfip=`ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p'`
echo GO TO http://$selfip:3333/
echo login: cds passwd: esq type: file

#rm -r /etc/motd
#echo "Run /srv/scripts/initial_install.sh" >/etc/motd

# меняем подключение к gitlab чтобы туда можно было коммитить
sed -i 's/url = https:\/\/gitlab.com\/cidious\/salt\.git/url = git@gitlab.com:cidious\/salt.git/' /srv/.git/config
cp /srv/salt/modules/git/files/.gitconfig /root/

# устанавливаем Far2l в систему
mkdir -p /root/.config/far2l/settings/
mkdir -p /root/.config/far2l/history/
cp -a /srv/salt/modules/far2l/settings/* /root/.config/far2l/settings/
cp -a /srv/salt/modules/far2l/history/* /root/.config/far2l/history/
cp -a /srv/salt/modules/far2l/files/* /usr/local/

# устанавливаем docker-compose
curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
chmod a+x /usr/local/bin/docker-compose
cp -a /usr/local/bin/docker-compose /srv/salt/

# обновляем всю систему, не задавая вопросов
export DEBIAN_FRONTEND=noninteractive
yes | apt -y upgrade

# устанавливаем lazydocker для миньонов
GITHUB_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/jesseduffield/lazydocker/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
GITHUB_FILE="lazydocker_${GITHUB_LATEST_VERSION//v/}_$(uname -s)_x86.tar.gz"
GITHUB_URL="https://github.com/jesseduffield/lazydocker/releases/download/${GITHUB_LATEST_VERSION}/${GITHUB_FILE}"
curl -L -o lazydocker.tar.gz $GITHUB_URL
tar xzf lazydocker.tar.gz lazydocker
sudo mv -f lazydocker /srv/salt
rm lazydocker.tar.gz

# конфигурируем swap для btrfs
device=`mount|grep ' / '|awk '{print $1}'`
mount $device /mnt
btrfs sub create /mnt/@swap
umount /mnt
mkdir /swap
mount -o subvol=@swap $device /swap
touch /swap/swapfile
chmod 600 /swap/swapfile
# выключаем Copy on Write для своп файла
chattr +C /swap/swapfile
# устанавливаем размер свопа 1Гб
dd if=/dev/zero of=/swap/swapfile bs=1M count=1024
mkswap /swap/swapfile
swapon /swap/swapfile
blkid=`blkid $device | cut -d '"' -f 2`
echo -e "\nUUID=$blkid /swap btrfs subvol=@swap 0 0\n/swap/swapfile none swap sw 0 0\n" >> /etc/fstab

# это нужно для wireguard
ln -s /usr/bin/resolvectl /usr/local/bin/resolvconf
# включаем wireguard
systemctl enable wg-quick@uahosting.service
systemctl start wg-quick@uahosting.service
echo Waiting for wireguard start...
sleep 3s

# устанавливаем nomad, vault, consul
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository -y "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
yes | apt -y install nomad consul vault
# копируем nomad и consul для миньонов
cp -a /usr/bin/nomad /srv/salt/
cp -a /usr/bin/consul /srv/salt/
cp -a /usr/bin/vault /srv/salt/
# выключаем wireguard
systemctl stop wg-quick@uahosting.service
systemctl disable wg-quick@uahosting.service

