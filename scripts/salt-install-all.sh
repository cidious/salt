#!/bin/bash

./salt-install.sh 192.168.9.252 prod1 cds
./salt-install.sh 192.168.9.253 dev cds

# salt-key -l un --no-color | grep -v Keys: | wc -l

# ждём пока миньоны не подцепятся к мастеру и запросят ключ
echo Sleeping 10 secs...
sleep 10s

salt-key -A -y
sleep 3s
salt '*' state.apply
