#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
yes | apt install -yqq cloud-image-utils
rm -f autoinstall-salt.iso
cloud-localds autoinstall-salt.iso user-data