#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
yes | apt install -yqq cloud-image-utils
rm -f autoinstall-dev.iso
cloud-localds autoinstall-dev.iso user-data