#!/bin/bash

if [ -z "$1" ] ; then
    echo Usage: $0 [ip-address] [minion-id] [ssh-user]
    exit
fi

# /etc/sudoers.d/adm : %adm ALL=NOPASSWD: ALL

ip="${1:-127.0.0.1}"
name="${2:-\$HOSTNAME}"
user="${3:-$USER}"

selfip=`ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p'`
port=22
ident=~/.ssh/id_rsa
pwdauth=no


cmd="ssh $ip -o User=$user -o Port=$port -o IdentityFile=$ident -o StrictHostKeyChecking=no \
    -o KbdInteractiveAuthentication=no -o GSSAPIAuthentication=no -o PasswordAuthentication=$pwdauth \
    -o ConnectTimeout=60 sudo sh -c \"apt -y install salt-minion; \
    dpkg -l salt-minion; \
    sed -i \\\"s/#id:/id: $name/g\\\" /etc/salt/minion;  \
    sed -i  's/#master: salt/master:  $selfip/g' /etc/salt/minion; \
    systemctl restart salt-minion \""

echo $cmd

`$cmd`

