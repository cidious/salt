users:
    cds:
         enabled: True
         groups:
            - adm
            - sudo
            - cdrom
            - dip
            - plugdev
            - lxd
            - docker
         uid: 1000
         ssh_auth_keys:
               - 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDd64ThgZA8X/EDd+PIwYOBDfq6LazCutshh96wY4PJM7hKJ9M3qoV/yhD0wQDwl/BGCo7ppk87MiYKaCYnzcdUyiUgVk9V/JVW+YIqQ8pl5+4X0uCNqfa2khG0l8G7nnzTlkkTtq32JpLB14W8oKgZbx6tQuVI8FQWQ+sYcF8ymVYkYSle26lZjrOzX2kx23Xad1HoF0wbu2GRfjqpbx28yLYtHKmICco+o+Q4pmVz8HpYPlL4E//vyKIvCdDOb4yClqgtfWXlPXpjVjWkmGp2KFGDU3OsL4siTzlDUaU52G8EFlzZITj9pqVM0G3V10QsbXAK+fINVR6sI3pMosTf'
         password: '$6$E1zSlTipX5Oop1HX$/n2oaqmYr/AmBaK9WIIXU5Y2wWu4wspVk7hFLogH7fJ2ANh6ta2PqqKw52L7LAKXbYg/P0d0DoL4exhP5nBVq/'
