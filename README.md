# salt automatic deployment

## Что это за проект?

Автоматически развёртывает всю инфраструктуру для разработки и продакшена с нуля.

1. автоматически устанавливает Ubuntu Server через cloud-config autoinstall
2. конфигурирует файловую систему Btrfs со своп-файлом на ней
3. ставит все нужные пакеты
4. создаёт юзеров и права
5. настраивает управляющий сервер с Saltstack, SaltGUI, Consul-server, Nomad-server
6. настраивает рабочий сервер с docker, docker-compose и девелоперским окружением, скачивает туда проект
7. создаёт продакшен-ноду с Nomad-worker и проектом в прод конфигурации

## Из каких компонентов состоит?

- скрипт для автоматизированной инсталяции Ubuntu Server (cloud-config). Он автоматически настраивает 3 сервера:
   1. управляющий сервер с Salt-server, Consul, Nomad-server, Vault
   2. сервер для разработки с Salt-minion, docker.io, docker-compose, lazydoker
   3. 1 или 2 сервера для продакшена с Salt-minion, Nomad-worker, gitlab-runner
- можно не устанавливать сервера для продакшена (если вам надо только программировать) или сервер для разработки (если вам нужен только продакшен)
- автоматическая инсталяция включает в себя:
   1. форматирование диска под [btrfs](https://habr.com/ru/company/veeam/blog/458250/) - прогрессивную файловую систему
   2. создание swap file в отдельном subvolume
   3. установку сети (netplan) с фиксированными, заранее прописанными IP/mask/gateway/DNS
   4. создание пользователя, под которым заходить и работать. У пользователя сразу прописан пароль, публичный ключ для ssh и секретный ключ для git pull/push, настройки git/far2l/bash

## Как его установить?

1. Установить [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2. Скачать 3 iso файла [autoinstall-salt.iso](https://gitlab.com/cidious/salt/-/raw/master/scripts/autoinstall-salt/autoinstall-salt.iso) [autoinstall-dev.iso](https://gitlab.com/cidious/salt/-/raw/master/scripts/autoinstall-dev/autoinstall-dev.iso) [autoinstall-prod.iso](https://gitlab.com/cidious/salt/-/raw/master/scripts/autoinstall-prod/autoinstall-prod.iso)
3. Скачать Ubuntu Server: [Option 2: Manual server installation](https://ubuntu.com/download/server)
4. Создать виртуальные машины:
   1. salt (и программисту и девопсу)
      - New
         - Name: salt
         - Type: Linux
         - Version: Ubuntu (64-bit)
         - Create Virtual Hard Disk - File size: 15 Gb
      - Settings
         - System - Motherboard - Base Memory: 2048 Mb
         - System - Procesors: 2 CPU
         - Storage - Controlller IDE: Primary 0: ubuntu-xx.yy-live-server-amd64.iso
         - Storage - Controlller IDE: Primary 1: autoinstall-salt.iso
         - Network - Adapter 1 - Attached to: Bridged Adapter
         - Network - Adapter 1 - Name: Intel(R) Dual Band Wireless-AC 7265
         - всё остальное по умолчанию
   2. dev (программисту)
      - New
         - Name: dev
         - Type: Linux
         - Version: Ubuntu (64-bit)
         - Create Virtual Hard Disk - File size: 20 Gb
      - Settings
         - System - Motherboard - Base Memory: 4096 Mb
         - System - Procesors: 3 CPU
         - Storage - Controlller IDE: Primary 0: ubuntu-xx.yy-live-server-amd64.iso
         - Storage - Controlller IDE: Primary 1: autoinstall-dev.iso
         - Network - Adapter 1 - Attached to: Bridged Adapter
         - Network - Adapter 1 - Name: Intel(R) Dual Band Wireless-AC 7265
         - всё остальное по умолчанию
   3. prod1 (девопсу)
      - New
         - Name: prod1
         - Type: Linux
         - Version: Ubuntu (64-bit)
         - Create Virtual Hard Disk - File size: 20 Gb
      - Settings
         - System - Motherboard - Base Memory: 4096 Mb
         - System - Procesors: 3 CPU
         - Storage - Controlller IDE: Primary 0: ubuntu-xx.yy-live-server-amd64.iso
         - Storage - Controlller IDE: Primary 1: autoinstall-prod.iso
         - Network - Adapter 1 - Attached to: Bridged Adapter
         - Network - Adapter 1 - Name: Intel(R) Dual Band Wireless-AC 7265
         - всё остальное по умолчанию
   4. prod2 (тут всё то же самое что в prod1)
5. Запустить ВМ salt
   1. меню GNU GRUB - Try or Installl Ubuntu Server - нажать Enter (или само прододлжит через 30сек)
   2. дальше начнётся автоматически процесс инсталяции, один раз он прервётся и спросит Continue with autoinstall? (yes|no) -- тут надо ввести yes[Enter]
   3. дальше процесс пойдёт полностью автоматически, надо подождать в зависимости от скорости компьютера, от 5 до 10 минут. Как только на экране всё бегать перестанет, значит процесс завершился, надо нажать [Enter] и должен появиться запрос `salt-auto login:`. После этого система инсталирована.
   4. залогиниться в `salt` под пользователем (по умолчанию юзер `cds` пароль `esq`) в консоли или в ssh, переключить в рута `sudo -i`
   5. скопировать свой секретный ключ `id_rsa` в `salt` в каталог `/srv/scripts/ssh/`
   6. перейти в каталог `cd /srv/scripts`, и запустить `./initial-install.sh`. Этот скрипт дважды спросит пароль. Первый раз он нужен для расшифровки секретного `id_rsa`. Этот ключ нужен чтобы скачать рабочий репозиторий проекта на `dev` сервере. Второй пароль нужен для расшифровки ключа для `wireguard`, который нужен для установки Consul/Nomad. Для разработки это не нужно.
   7. открываем в бравзере [SaltGUI](http://192.168.9.254:3333/) находящийся на salt сервере на порту 3333
6. Запустить ВМ dev
   1. Всё то же самое что было для `salt` -- установить сервер, дождаться завершения
   2. В итоге должно работать минимум 2 виртуалки: (salt+dev) или (salt+prod) или все вместе.
7. в консоли или в ssh-терминале сервера `salt` дать команду `./salt-install-all.sh`. Эта команда установит `salt-minion` на все остальные сервера. Пакет `salt-minion` нужен для автоматической настройки всех оставшихся компонентов. `Salt` выступает управляющим сервером, остальные сервера подчинённые.

## Как использовать?

1. скачать ssh-клиент. Я рекомендую [Putty](https://github.com/unxed/putty4far2l) заточенный под `far2l` или чуть более продвинутый [Kitty](https://github.com/mihmig/KiTTY/releases) (также заточенный). Также альтернативный вариант [MobaXterm](https://mobaxterm.mobatek.net/) -- там есть великолепный протокол `mosh` облегчающий работу на медленных или нестабильных соединениях, то в нём не удобно работать в `far2l`.
   1. Как сконфигурировать `Putty/Kitty`:
      1. Session - Host Name (or IP address): `192.168.9.253`
      2. Port: `22`
      3. Connection type: `SSH`
      4. Window - Columns: `140`, Rows: `50`
      5. Window - Appearance - `Font: Hack, 10-point`
      6. Window - Behaviour - Window Title: `%%h`
      7. Window - Translation - Remote character set: `UTF-8`
      8. Window - Selection - `Compromise`, `[√]` Auto-copy selected text
      9. Connection - Data - Auto-login username: `cds`
      10. Connection - SSH - Auth - Private key file for auth: `ваш приватный ключ в формате ppk`
   2. Как сконвертировать ваш приватный ключ в обычном линуксовом формате OpenSSL в формат Putty?
      1. Скачать [puttygen.exe](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) -- найти ссылку на `64-bit x86: puttygen.exe`
      2. запустить `puttygen.exe`
      3. Верхнее меню Conversions - Import key
      4. загрузить ваш `id_rsa`
      5. нажать справа кнопку `Save private key` (на вопрос `Are you sure .. without a passphrase` ответить `Yes`)
      6. ввести имя файла `id_rsa.ppk`, сохранить.
2. `far2l` это тот самый [Far Manager](https://www.farmanager.com/index.php?l=ru) заточенный под Линукс. Там работают все клавиши, общий clipboard, он сохраняет состояние при обрыве соединения. Автоматический инсталятор установит `far2l` на все сервера. Вот [список шорткатов](https://cheatography.com/alexzaitzev/cheat-sheets/far-3/) от Far3, он в большинстве случаев подходит для far2l.
