base:
  'not *.test':
    - match: compound
    - modules.git
    - modules.vim
    - modules.net-tools
    - modules.groups
    - modules.users
    - modules.tcpdump
    - modules.htop
    - modules.samba
    - modules.far2l
    - modules.btrfsswap
    - modules.docker

  'dev*':
    - modules.dev
    - modules.lazydocker
    - modules.docker-compose
    - modules.jq

#  'prod*':
#    - modules.prod
