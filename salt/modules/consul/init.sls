# https://community.hetzner.com/tutorials/install-nomad-consul-cluster

# add autocomplete functionality for Consul
# consul -autocomplete-install
# complete -C /usr/local/bin/consul consul

# Create a user for Consul
# useradd --system --home /etc/consul.d --shell /bin/false consul
# mkdir --parents /opt/consul
# chown --recursive consul:consul /opt/consul

# Prepare the Consul configuration
# mkdir --parents /etc/consul.d
# touch /etc/consul.d/consul.hcl
# chown --recursive consul:consul /etc/consul.d
# chmod 640 /etc/consul.d/consul.hcl

# Prepare the TLS certificates for Consul
# consul tls ca create
# consul tls cert create -server -dc dc1
# consul tls cert create -client -dc dc1

# create a symmetric encryption key, which will be shared between all servers
# echo `consul keygen` > consul.key

# copy the right certificates from step 1 to the Consul configuration directory
# cp consul-agent-ca.pem /etc/consul.d/

#  $ ls /etc/consul.d/
#  consul-agent-ca.pem
#  consul.hcl
#  dc1-server-consul-X-key.pem
#  dc1-server-consul-X.pem

# rm *.pem

# consul validate /etc/consul.d/consul.hcl

# create the configuration file /etc/consul.d/server.hcl with the following content
#  server = true
#  bootstrap_expect = 3

# systemctl start consul
# consul members
