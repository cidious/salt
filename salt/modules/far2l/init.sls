installforf2l:
  pkg.installed:
    - pkgs:
      - chafa
      - libneon27
      - libspdlog1
      - libpcre3
      - libuchardet0
      - libarchive13
      - libxerces-c3.2
      - libfmt8

install_far2l:
  file.recurse:
    - name: /usr/local
    - source: salt://modules/far2l/files
    - clean: False
    - user: root
    - group: root
    - if_missing: /usr/local/bin/far2l
    - file_mode: keep

/home/cds/.config:
  file.directory:
    - user: cds
    - group: cds
    - mode: 755

f2lsettings:
  file.recurse:
    - source: salt://modules/far2l/settings
    - name: /home/cds/.config/far2l/settings
    - makedirs: True
    - replace: False
    - unless: ls /home/cds/.config/far2l/settings

f2lhistory:
  file.recurse:
    - source: salt://modules/far2l/history
    - name: /home/cds/.config/far2l/history
    - makedirs: True
    - replace: False
    - unless: ls /home/cds/.config/far2l/history
