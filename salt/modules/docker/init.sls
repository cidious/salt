installfordocker:
  pkg.installed:
    - pkgs:
      - docker.io

/etc/systemd/system/docker.service.d/override.conf:
  file.managed:
    - source: salt://modules/docker/files/override.conf
    - user: root
    - group: root
    - mode: 0644
    - makedirs: true
  cmd.run:
    - name: systemctl daemon-reload; systemctl restart docker
    - onchanges:
      - file: /etc/systemd/system/docker.service.d/override.conf

docker-server:
  service.running:
    - name: docker
    - enable: True
    - reload: True
