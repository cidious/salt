# Add autocomplete functionality to nomad (optional)
# nomad -autocomplete-install
# complete -C /usr/local/bin/nomad nomad

# Prepare the data directory
# mkdir --parents /opt/nomad

# Create the basic configuration file for nomad
# mkdir --parents /etc/nomad.d
# chmod 700 /etc/nomad.d
# touch /etc/nomad.d/nomad.hcl

# Add the following configuration in the file /etc/nomad.d/nomad.hcl
# datacenter = "dc1"
# data_dir = "/opt/nomad"

# nomad acl bootstrap
# export NOMAD_TOKEN="your-secret-id"
# nomad server members
