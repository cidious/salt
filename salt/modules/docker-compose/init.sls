copydockercompose:
  file.managed:
    - source: salt://docker-compose
    - user: root
    - group: root
    - mode: 0555
    - name: /usr/local/bin/docker-compose
