{% for group, args in pillar['groups'].items() %}
{{ group }}_group:
    group.present:
        - name: {{ group }}
        {% if 'gid' in args %}
        - gid: {{ args['gid'] }}
        {% endif %}
        {% if 'system' in args %}
        - system: {{ args['system'] }}
        {% endif %}
{% endfor %}
