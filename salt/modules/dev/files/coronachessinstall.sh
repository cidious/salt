#!/bin/bash

if [[ ! -f ~/.ssh/known_hosts ]] || [[ $(grep -L gitlab ~/.ssh/known_hosts) ]] ; then
  ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
fi

git clone git@gitlab.com:cidious/coronachess.git /home/cds/dev/coronachess
