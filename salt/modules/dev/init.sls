dev_dir:
  file.directory:
    - name: /home/cds/dev
    - user: cds
    - group: cds

copyidrsa:
  file.managed:
    - source: salt://id_rsa
    - user: cds
    - group: cds
    - mode: 0600
    - name: /home/cds/.ssh/id_rsa

coronachess:
  cmd.script:
    - source: salt://modules/dev/files/coronachessinstall.sh
    - cwd: /home/cds/dev
    - runas: cds
    - unless: ls /home/cds/dev/coronachess && ls ~/.ssh/id_rsa && test ! -d /home/cds/dev/.git

#startproject:
#  cmd.script:
#    - source: salt://modules/dev/files/coronachessstart.sh
#    - cwd: /home/cds/dev/coronachess
#    - user: cds
#    - unless: ls /home/cds/dev/coronachess-database

gitconfig:
  file.managed:
    - source: salt://modules/dev/files/.gitconfig
    - user: cds
    - group: cds
    - mode: 0664
    - name: /home/cds/.gitconfig


# как создать файл взяв содержимое прямо из sls  
#/etc/systemd/system/nginx.service.d/nofile_limit.conf:
#  file.managed:
#    - require:
#        - file: /etc/systemd/system/nginx.service.d
#    - contents: |
#        [Service]
#        LimitNOFILE=16384

# Install httpie
# https://github.com/httpie/httpie
#curl -SsL https://packages.httpie.io/deb/KEY.gpg | apt-key add -
#curl -SsL -o /etc/apt/sources.list.d/httpie.list https://packages.httpie.io/deb/httpie.list
#apt update
#apt install httpie

# gping: Ping, but with a graph.
# https://github.com/orf/gping
#echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
#wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
#sudo apt update
#sudo apt install gping

