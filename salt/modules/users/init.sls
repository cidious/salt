{% for username, data in pillar.get('users', {}).items() %}
/home/{{ username }}:
    file:
        - directory
        - user: {{ username }}
        - group: {{ username }}
        - mode: 0700
        - require:
            - user: {{ username }}
            - group: {{ username }}

/home/{{ username }}/.ssh:
    file:
        - directory
        - user: {{ username }}
        - group: {{ username }}
        - mode: 0700
        - require:
            - user: {{ username }}
            - group: {{ username }}
            - file: /home/{{ username }}

{{ username }}:
    group:
        - present
        - name: {{ username }}
        - gid: {{ data.get('uid') }}

    user:
        - present
        - name: {{ username }}
        - uid: {{  data.get('uid') }}
        - gid: {{ data.get('uid') }}
        - password: {{ data.get('password') }}
        {% if data.get('enabled') %}
        - shell: /bin/bash
        {% else %}
        - shell: /sbin/nologin
        {% endif%}
        - groups:
            - {{ username }}
            {% for group in data.get('groups') %}
            - {{ group }}
            {% endfor %}
        - home: /home/{{ username }}
        - require:
            - group: {{ username }}

{% for ssh_auth_key in data.get('ssh_auth_keys', '') %}
{{ username }}_ssh_key_{{ loop.index }}:
    ssh_auth:
        - present
        - user: {{ username }}
        - name: {{ ssh_auth_key }}
        - require:
            - user: {{ username }}
            - file: /home/{{ username }}/.ssh
{% endfor %}
{% endfor %}
