samba-pkgs:
    pkg.installed:
        - pkgs:
            - samba

/etc/samba/smb.conf:
    file.managed:
        - source: salt://modules/samba/files/smb.conf
        - user: root
        - group: root
        - mode: 644
smbpasswd-add-cmd:
    cmd.run:
        - name: bash -c "(echo esq; echo esq) | smbpasswd -s -a cds ; systemctl restart smbd"
        - unless: grep "cds:" /etc/samba/smbpasswd
