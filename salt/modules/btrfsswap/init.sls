swaprebuild:
  cmd.script:
    - source: salt://modules/btrfsswap/createswap.sh
    - cwd: /
    - user: root
    - unless: ls /swap
