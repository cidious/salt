#!/bin/bash

# определяем имя рутового девайса
device=`mount|grep ' / '|awk '{print $1}'`

# выключаем существующий своп
swapoff -a
sed -ie '/\/swap.img/s/^/#/g' /etc/fstab
rm -f /swap.img

# создаём отдельный сабвольюм для свопа чтобы можно было делать снапшоты на руте
mount $device /mnt
btrfs sub create /mnt/@swap
umount /mnt
mkdir /swap
mount -o subvol=@swap $device /swap

# создаём новый своп
touch /swap/swapfile
chmod 600 /swap/swapfile

# выключаем Copy on Write для своп файла
chattr +C /swap/swapfile

# устанавливаем размер свопа 1Гб
dd if=/dev/zero of=/swap/swapfile bs=1M count=1024
mkswap /swap/swapfile
swapon /swap/swapfile

# прописываем своп в fstab
blkid=`blkid $device | cut -d '"' -f 2`
echo -e "\nUUID=$blkid /swap btrfs subvol=@swap 0 0\n/swap/swapfile none swap sw 0 0\n" >> /etc/fstab
