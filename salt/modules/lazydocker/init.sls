copylazydocker:
  file.managed:
    - source: salt://lazydocker
    - user: root
    - group: root
    - mode: 0555
    - name: /usr/local/bin/lazydocker
